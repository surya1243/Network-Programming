#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

void diewitherrormsg(char *message){
	printf("%s\n",message);
	exit(1);
}

int main(){

	int returncode;
	const int MAXBUFFERSIZE = 10;
	char readbuffer[MAXBUFFERSIZE+1];	
	int bytesrecvd;
	time_t current_time;

	//1. allocate a socket;
	int mysocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(mysocket < 0) diewitherrormsg("socket() failed.");

	//2. fix serveradddress (using bind());
	struct sockaddr_in serveraddress;
	bzero(&serveraddress, INET_ADDRSTRLEN);
	serveraddress.sin_family = AF_INET;
	serveraddress.sin_port = htons(12345);
	returncode = inet_pton(AF_INET, "127.0.0.1", &serveraddress.sin_addr.s_addr);
	//returncode = serveraddress.sin_addr.s_addr = htonl(INADDR_ANY);
	//We can specify the IP address as INADDR_ANY, which allows the server to accept a client connection on any interface
	
	//the server's well-known port (13 for the daytime service) is bound to the socket
	returncode = bind(mysocket, (struct sockaddr *)&serveraddress,INET_ADDRSTRLEN);
	if(returncode < 0) diewitherrormsg("bind() failed.");
	//3. listen() in passive mode;
	returncode = listen(mysocket,0);
	if(returncode < 0 ) diewitherrormsg("listen() failed.");
	while(1){
	//4. accept() new connection;
	int newsocket = accept(mysocket,NULL,0);
	if(newsocket < 0) diewitherrormsg("accept() failed.");
	//bytesrecvd = recv(newsocket, readbuffer, MAXBUFFERSIZE,0);
	
	
	time(&current_time);
	//send(newsocket,sizeof(current_time),sizeof(size_t));
	send(newsocket, &current_time, sizeof(current_time),0);

	//5. close() new connection;
	//The server closes its connection with the client by calling close.
	close(newsocket);

	//6. repeat
	

	}

	

	return 0;
}
