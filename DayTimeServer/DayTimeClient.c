#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define MAXBUFFERSIZE 20
void diewitherrormsg(char * message){
	printf("%s\n",message);
	exit(1);
}
int main(){
	
	struct sockaddr_in serveraddress;
	int returncode;
	char message[] = "hello";
	char readbuffer[MAXBUFFERSIZE+1];
	int bytesrecvd;
	time_t recvd_time = 0;

	//1. allocate a socket
	int mysocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(mysocket < 0) diewitherrormsg("socket() failed.");

	//2. fill up the remote address structure
	bzero(&serveraddress,sizeof(serveraddress));
	serveraddress.sin_family = AF_INET;      	/* Internet address family */
	serveraddress.sin_port = htons(12345);		/* Server port */
	returncode = inet_pton(AF_INET, "127.0.0.1",&serveraddress.sin_addr.s_addr);
	if(returncode == 0) diewitherrormsg("inet_aton() failed.");
	
	//3. call connect()
	returncode = connect(mysocket,(struct sockaddr *) &serveraddress, INET_ADDRSTRLEN);
	if(returncode < 0) diewitherrormsg("connect() failed.");

	//4. read() / write()	
	bytesrecvd = recv(mysocket, &recvd_time, sizeof(recvd_time),0);
	
	//ctime : transform date and time to broken-down time or ASCII for better readable
	printf("Received %s from server.\n",ctime(&recvd_time));


	//5. close()
	close(mysocket);



	return 0;
}
