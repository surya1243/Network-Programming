			
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
void diewitherrormessage(char *message){
	printf("%s\n",message);
	exit(1);
}
int main(){

	int returncode;
	
	//1. allocate socket()
	int mysocket = socket( AF_INET, SOCK_STREAM, 0 );
	if(mysocket < 0){
		diewitherrormessage("socket() failed");
	}
	//	2. define where i want to connect
	struct sockaddr_in serveraddress;
	memset(&serveraddress,0,sizeof(serveraddress));
	serveraddress.sin_port =htons( 12345);
	serveraddress.sin_family = AF_INET;
	//	serveraddress.sin_addr.s_addr = "127.0.0.1";
	returncode = inet_pton(AF_INET,"127.0.0.1",&serveraddress.sin_addr.s_addr);
	if(returncode == -1){
		printf("Could not convert to binary format. inet_pton() failed");
		exit(1);
	}
	//	3. connect() actively create a connection
	returncode = connect(mysocket, (struct sockaddr *)&serveraddress , sizeof(serveraddress));
	if(returncode == -1){
		diewitherrormessage("connect() failed");
	}
	//	4. send() recv()  read() write()
		char message[] = "hello";
		send(mysocket, message,sizeof(message),0);
		const int MAXBUFFERSIZE = 10;
		char buffer[MAXBUFFERSIZE+1];
		int bytesrecvd = recv(mysocket,buffer,MAXBUFFERSIZE,0);
		buffer[bytesrecvd] = '\0';
		printf("Received %s from server",buffer);
//	5. close()
	close(mysocket);
	return 0;
}
