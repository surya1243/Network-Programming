#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
void diewitherrormessage(char * message){
	printf("%s\n",message);
	exit(1);
}
int main(){

	//1. allocate a socket(socket());
	int mysocket	 = socket(AF_INET,SOCK_STREAM,0);

	//	2. create socket address structure;
	struct sockaddr_in serveraddress;
	memset(&serveraddress,0,sizeof(serveraddress));
	serveraddress.sin_family = AF_INET;
	serveraddress.sin_port = htons(12345);
	serveraddress.sin_addr.s_addr = htonl(INADDR_ANY);

	//3. set the socket into passive mode ( bind );
	int returncode;
	returncode = bind(mysocket,(struct sockaddr *)&serveraddress, sizeof(serveraddress));
	if(returncode == -1){
		diewitherrormessage("bind() failed");
	}
 	listen(mysocket,0);	
	for(;;){
	//4. accept() incoming connection from client;
		struct sockaddr_in clientaddress;
		int clientaddr_len;
		int newsocket = accept(mysocket,(struct sockaddr *) &clientaddress,&clientaddr_len);
		if(newsocket < 0){
			diewitherrormessage("accept() failed");
		}
	//5. send() /recieve() in a different socket;
		const int MAXBUFFERSIZE = 30;
		char buffer[MAXBUFFERSIZE+1];
		int bytesrecvd = recv(newsocket, buffer,sizeof(buffer),0);

		send(newsocket,buffer,bytesrecvd,0);
	//6. close() the socket;
		close(newsocket);
	//7. go back to accepting new connection request from client;


	}
		

	return 0;
}
