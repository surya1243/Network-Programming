#include "PracticalSocket.h"
#include <iostream>
int main(){

	unsigned short listeningPort = 12345;
	UDPSocket mySocket(listeningPort);

	const int RECVBUFFERSIZE = 32;
	char recvBuffer[RECVBUFFERSIZE+1];
	string senderIP;
	unsigned short senderPort;
	//receive
	int bytesreceived = mySocket.recvFrom(recvBuffer,RECVBUFFERSIZE,senderIP,senderPort );
	

	std::cout<<mySocket.getLocalAddress()<<":"<<mySocket.getLocalPort()<<" received from "<<senderIP<<":"<<senderPort<<" :"<<recvBuffer<<'\n';


	//send
	mySocket.sendTo(recvBuffer,bytesreceived,senderIP,senderPort);
	std::cout<<mySocket.getLocalAddress()<<":"<<mySocket.getLocalPort()<<" sent to "<<senderIP<<":"<<senderPort<<" :"<<recvBuffer<<'\n';

	return 0;
}