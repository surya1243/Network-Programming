#include <iostream>
#include "PracticalSocket.h"
int main(){

	UDPSocket mySocket;

	string message = "Hello";
	string serverIP = "127.0.0.1";
	unsigned short serverPort = 12345;
	
	//send
	mySocket.sendTo(message.c_str(),message.length(), serverIP, serverPort );
	std::cout<<mySocket.getLocalAddress()<<":"<<mySocket.getLocalPort()<<" sent to "<<serverIP<<":"<<serverPort<<" :"<<message<<'\n';

	const int RECVBUFFERSIZE = 32;
	char recvBuffer[RECVBUFFERSIZE+1];
	string senderIP;
	unsigned short senderPort;
	//receive
	int bytesreceived = mySocket.recvFrom(recvBuffer,RECVBUFFERSIZE, senderIP, senderPort );
	
	recvBuffer[bytesreceived] = '\0';

	std::cout<<mySocket.getLocalAddress()<<":"<<mySocket.getLocalPort()<<" received from "<<senderIP<<":"<<senderPort<<" :"<<recvBuffer<<'\n';

	return 0;
}