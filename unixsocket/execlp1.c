#include <stdio.h>
#include <unistd.h>

int main(){
	
	int pd[2]; //pipe descriptor

	pipe(pd); //create pipe

	if(fork() == 0){
		close(1);  //Close unused pipe descriptors
		dup(pd[1]); //the output is now sent to pipe
		close(pd[0]); // we are not going to read from pipe
		//close the read side of the pipe
		
		execlp("ls","ls","-l",NULL);
		
		//Calling one of the exec() family will terminate the currently running program and
		//starts executing a new one which is specified in the parameters of exec in the
		//context of the existing process.
		
		//child
	}else{
		close(0);
		dup(pd[0]); //the input in now recvd from pipe
		close(pd[1]); //we are not going to write to the pipe
		//close the write side of the pipe
		
		//parent
		execlp("wc","wc","-l",NULL);
	}

	return 0;
}