#include <unistd.h>
#include <stdio.h>

int main(){


	int var;

	if (fork() == 0){
		//child
		//When a program wants to have another program running in parallel, it will typically first use fork 
	
		var = 5;
		printf("child:%d\n",var);
	}else{
		//parent
		printf("parent:%d\n",var);

	}
}