#include <sys/socket.h>
#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
int main(){

	//Unix domain sockets are used when passing descriptors between processes on the same host.	
	
	int socketvector[2]; //pair of socket descriptors
	
	//socketpair() function creates two sockets that are connected together.
	socketpair(AF_LOCAL,SOCK_STREAM,0,socketvector);
	//int socketpair(int family, int type, int protocol, int sockfd[2]);
	
	//The result of scoketpair() with a type of SOCK_STREAM is called a stream pipe. The stream pipe
	//is full-duplex; that is, both descriptors can be read and written.
	
	char readbuffer;

	if( fork() == 0){
		//child
		read(socketvector[0],&readbuffer,1);
		printf("Child: read %c \n",readbuffer );
		readbuffer = toupper(readbuffer);
		write(socketvector[0],&readbuffer,1);
		printf("Child: sent %c \n",readbuffer);
	}else{
		//parent
		write(socketvector[1],"b",1);
		printf("Parent: sent 'b' \n");
		read(socketvector[1],&readbuffer,1);
		printf("Parent: read %c \n", readbuffer);
		wait(NULL); /* wait for child to exit() */
	}
	

	return 0;
}